use futures::future::FutureObj;
use lavalink::Error as LavalinkError;
use reqwest::{Error as ReqwestError, UrlError};
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    num::ParseIntError,
    result::Result as StdResult,
};

pub type FutureResult<T> = FutureObj<'static, T>;
pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    /// An indicator that the server responded with a non-404 4xx status.
    BadRequest,
    /// An indicator that the server responded with a 5xx status.
    InternalServerError,
    /// An error from the `lavalink` crate.
    Lavalink(LavalinkError),
    /// An indicator that the server responded with a 404 status.
    NotFound,
    ParseInt(ParseIntError),
    /// An error from the `reqwest` crate.
    Reqwest(ReqwestError),
    /// An error occurred while parsing a `reqwest` `Url`.
    Url(UrlError),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_str(self.description())
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        use self::Error::*;

        match self {
            BadRequest => "4xx error",
            Error::InternalServerError => "5xx error",
            Error::Lavalink(e) => e.description(),
            Error::NotFound => "404 error",
            Error::ParseInt(e) => e.description(),
            Error::Reqwest(e) => e.description(),
            Error::Url(e) => e.description(),
        }
    }
}

impl From<LavalinkError> for Error {
    fn from(err: LavalinkError) -> Self {
        Error::Lavalink(err)
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Self {
        Error::ParseInt(err)
    }
}

impl From<ReqwestError> for Error {
    fn from(err: ReqwestError) -> Self {
        Error::Reqwest(err)
    }
}

impl From<UrlError> for Error {
    fn from(err: UrlError) -> Self {
        Error::Url(err)
    }
}
