use lavalink::decoder::DecodedTrack;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default)]
pub struct GetRequest {
    pub guild_id: u64,
    pub limit: Option<u64>,
    pub offset: Option<u64>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct QueuedItem {
    pub song_id: i64,
    pub song_author: String,
    pub song_identifier: String,
    pub song_length: i64,
    pub song_source: String,
    pub song_stream: bool,
    pub song_title: String,
    pub song_track: String,
    pub song_url: Option<String>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct SongData {
    pub id: i64,
    pub track: String,
}

#[derive(Debug)]
pub struct Song {
    pub info: DecodedTrack,
    pub track_base64: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct SongQueuedData {
    pub guild_id: String,
    pub song_id: String,
    pub track: String,
}

#[derive(Debug)]
pub struct SongQueued {
    pub guild_id: String,
    pub song_id: String,
    pub info: DecodedTrack,
    pub track_base64: String,
}
