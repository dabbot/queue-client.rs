use crate::{
    error::{Error, Result},
    model::{GetRequest, Song, SongQueued, SongQueuedData},
};
use futures::compat::Future01CompatExt;
use lavalink::decoder;
use log::{debug, warn};
use reqwest::r#async::Client as ReqwestClient;
use serde::de::DeserializeOwned;
use serde_json::{self, Value, json};
use std::sync::Arc;

pub struct Client {
    host: String,
    inner: Arc<ReqwestClient>,
}

impl Client {
    pub fn new(client: Arc<ReqwestClient>, host: impl ToString) -> Self {
        Self {
            host: host.to_string(),
            inner: client,
        }
    }

    async fn empty<'a>(
        &'a self,
        call: impl AsRef<str> + 'a,
        data: Value,
    ) -> Result<()> {
        await!(self._empty(call.as_ref(), data))
    }

    async fn _empty<'a>(
        &'a self,
        call: &'a str,
        data: Value,
    ) -> Result<()> {
        let url = http_client_base::url(false, &self.host, "/", None)?;
        let body = body(call, data);

        let resp = await!(self.inner.post(url).json(&body).send().compat())?;

        check_status(resp.status().as_u16())?;

        Ok(())
    }

    async fn send<'a, T: DeserializeOwned>(
        &'a self,
        call: &'a str,
        data: Value,
    ) -> Result<T> {
        let url = http_client_base::url(false, &self.host, "/", None)?;
        let body = body(call, data);

        debug!("URL: {}", url);
        debug!("Body: {}", body);

        let mut resp = await!(self.inner.post(url).json(&body).send().compat())?;

        check_status(resp.status().as_u16())?;

        await!(resp.json().compat()).map_err(From::from)
    }

    pub async fn add_track<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
        track: impl ToString + 'a,
    ) -> Result<SongQueued> {
        await!(self.add_at(guild_id, track, "V1_GUILDS_ADD"))
    }

    pub async fn add_start<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
        track: impl ToString + 'a,
    ) -> Result<SongQueued> {
        await!(self.add_at(guild_id, track, "V1_GUILDS_ADD_START"))
    }

    async fn add_at<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
        track: impl ToString + 'a,
        call: impl AsRef<str> + 'a,
    ) -> Result<SongQueued> {
        let guild_id = guild_id.to_string();

        let data = json!({
            "guild_id": guild_id,
            "track": track.to_string(),
        });

        let song = await!(self.send::<SongQueuedData>(
            call.as_ref(),
            data,
        ))?;
        let decoded = decoder::decode_track_base64(&song.track)?;

        Ok(SongQueued {
            guild_id: song.guild_id,
            song_id: song.song_id,
            info: decoded,
            track_base64: song.track,
        })
    }

    pub async fn add_tracks<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
        tracks: Vec<impl ToString + 'a>,
    ) -> Result<Vec<SongQueued>> {
        let tracks = tracks.into_iter().map(|track| {
            track.to_string()
        }).collect::<Vec<_>>();

        let data = json!({
            "guild_id": guild_id.to_string(),
            "tracks": tracks,
        });

        let datasets = await!(self.send::<Vec<SongQueuedData>>(
            "V1_GUILDS_ADD_BATCH",
            data,
        ))?;

        let queued = datasets.into_iter().filter_map(|data| {
            let decoded = decoder::decode_track_base64(&data.track).ok()?;

            Some(SongQueued {
                guild_id: data.guild_id,
                song_id: data.song_id,
                info: decoded,
                track_base64: data.track,
            })
        }).collect::<Vec<SongQueued>>();

        Ok(queued)
    }

    pub async fn delete_track<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
        track: impl ToString + 'a,
    ) -> Result<()> {
        await!(self.delete_tracks(guild_id, vec![track]))
    }

    pub async fn delete_tracks<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
        tracks: Vec<impl ToString + 'a>,
    ) -> Result<()> {
        let tracks = tracks.into_iter().map(|track| {
            track.to_string()
        }).collect::<Vec<_>>();

        let body = json!({
            "guild_id": guild_id.to_string(),
            "tracks": tracks,
        });
        await!(self.empty("V1_GUILDS_DELETE", body))?;

        Ok(())
    }

    pub async fn delete_queue<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
    ) -> Result<()> {
        let body = json!({
            "guild_id": guild_id.to_string(),
        });

        await!(self.empty("V1_GUILDS_DELETE_QUEUE", body))?;

        Ok(())
    }

    pub async fn get(&self, req: GetRequest) -> Result<Vec<Song>> {
        let body = json!({
            "guild_id": req.guild_id,
            "limit": req.limit,
            "offset": req.offset,
        });

        let base64s = await!(self.send::<Vec<String>>(
            "V1_GUILDS_GET",
            body,
        ))?;

        let decoded_tracks = base64s.into_iter().filter_map(|b64| {
            let info = decoder::decode_track_base64(&b64).ok()?;

            Some(Song {
                track_base64: b64,
                info,
            })
        }).collect();

        Ok(decoded_tracks)
    }

    pub async fn pop<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
    ) -> Result<Option<Song>> {
        let body = json!({
            "guild_id": guild_id.to_string(),
        });

        let resp = await!(self.send::<String>(
            "V1_GUILDS_POP",
            body,
        ));

        let track = match resp {
            Ok(song) => song,
            Err(Error::Reqwest(ref why)) if why.is_serialization() => {
                return Ok(None);
            },
            Err(why) => return Err(why),
        };

        let decoded = decoder::decode_track_base64(&track)?;

        Ok(Some(Song {
            info: decoded,
            track_base64: track,
        }))
    }

    pub async fn reorder<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
        from: u64,
        to: u64,
    ) -> Result<()> {
        let body = json!({
            "guild_id": guild_id.to_string(),
            "from": from,
            "to": to,
        });

        await!(self.empty("V1_GUILDS_REORDER", body))?;

        Ok(())
    }

    pub async fn shuffle<'a>(
        &'a self,
        guild_id: impl ToString + 'a,
    ) -> Result<()> {
        let body = json!({
            "guild_id": guild_id.to_string(),
        });

        await!(self.empty("V1_GUILDS_SHUFFLE", body))?;

        Ok(())
    }
}

fn body(call: &str, data: Value) -> Value {
    json!({
        "key": call,
        "data": data,
    })
}

fn check_status(status: u16) -> Result<()> {
    match status {
        404 => Err(Error::NotFound),
        400 ... 403 | 405 ... 499 => Err(Error::BadRequest),
        500 ... 599 => Err(Error::InternalServerError),
        200 ... 299 => Ok(()),
        other => {
            warn!("Got odd response status: {}", other);

            Err(Error::InternalServerError)
        },
    }
}
