#![feature(await_macro, futures_api, async_await)]

pub mod model;

mod client;
mod error;

pub use self::{
    client::Client as QueueClient,
    error::{Error, FutureResult, Result},
};
